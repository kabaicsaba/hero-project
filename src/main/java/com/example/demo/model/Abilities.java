package com.example.demo.model;

public class Abilities {

	private String name;
	private String id;
	
	public Abilities() {}
	
	public Abilities(String name, String id) {
		this.name=name;
		this.id=id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Abilities [name="+name+", id="+id+ " ]";
	}
}
