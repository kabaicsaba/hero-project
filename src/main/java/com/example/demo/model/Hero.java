package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

public class Hero {

	//first
	private String id;
	private String name;
	private int age;
	private int height;
	private int weight;
	//second
	private String origin;
	private String sex;
	private String description;
	private String type;
	//third
	private String profilpic;
	private List<Abilities> abilities = new ArrayList<>(); 

	public Hero() {	}
	
	public Hero(String id, String name, int age, int height, int weight) {
		this.id=id;
		this.name=name;
		this.age=age;
		this.height=height;
		this.weight=weight;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "Hero [id="+id+", name="+name+", age="+age+", height="+height+", weight="+weight+" ]";
	}
}
